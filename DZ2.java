import java.util.Scanner;

public class DZ2 {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
       System.out.println("Введи количество элементов в массиве, а также их самих");
        int n=in.nextInt();
        int[] mas=new int[n];
        for (int i = 0; i < n; i++) mas[i]=in.nextInt();
      System.out.println("Введи число, благодаря которому будут отсеиваться элементы массива");
        int max=in.nextInt();
        int count=0;
        for (int i = 0; i < n; i++) {
            if(mas[i]<max){
                mas[i]=0;
                count++;
            }
        }
        for (int i = 0; i < n; i++) {
            if (mas[i] == 0) {
                int c = 0;
                if(n-i==count) break;
                int j = i;
                while (mas[j] == 0) {
                    c++;
                    j++;
                }
                mas[i] = mas[j];
                mas[j] = 0;
            }
        }
        System.out.println("Изменённый массив:");
        for (int i = 0; i < n; i++) System.out.print(mas[i] + " ");
        System.out.println();
       System.out.println("Сортировка массива:");
        int minid=0;
        for (int i = 0; i < n; i++) {
            minid=i;
            for (int j = i; j < n; j++) {
                if(mas[j]<mas[minid]) minid=j;
            }
            if(minid!=i){
                mas[minid]+=mas[i];
                mas[i]=mas[minid]-mas[i];
                mas[minid]-=mas[i];
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(mas[i]+" ");
        }
    }
}
