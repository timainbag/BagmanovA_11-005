import java.util.Scanner; 
public class QuickSort { 
 public static void main(String[] args) { 
 Scanner in = new Scanner(System.in); 
 int x = in.nextInt(); 
 int [] a = new int[x]; 
 for ( int i = 0; i<a.length; i++){ 
 a[i] = in.nextInt(); 
 } 
 qS(a, 0, a.length-1); 
 arrayToString(a); 
 } 
 private static void qS(int[] arr, int f, int t) { 
 if (f < t) { 
 int splitIndex = partition(arr, f, t); 
 qS(arr, f, splitIndex -1); 
 qS(arr, splitIndex, t); 
 } 
 } 
 private static int partition(int[] arr, int f, int t) { 
 int l= f; 
 int r = t; 
 int pivot = arr[f+(t-f)/2]; 
 while (l <= r) { 
 while (arr[l] < pivot) { 
 l++; 
 } 
 while (arr[r] > pivot) { 
 r--; 
 } 
 
 if (l <= r) { 
 swap(arr, r, l); 
 l++; 
 r--; 
 
 } 
 } 
 return l; 
 } 
 
 private static void swap(int[] arr, int index1, int index2) { 
 int tmp = arr[index1]; 
 arr[index1] = arr[index2]; 
 arr[index2] = tmp; 
 
 } 
 private static void arrayToString(int[] a) { 
 for (int i=0;i<a.length;i++) 
 System.out.print(a[i] + " "); 
 System.out.println(); 
 } 
}